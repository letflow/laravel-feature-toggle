<?php

namespace Kirschbaum\LaravelFeatureFlag;


use Illuminate\Support\Collection;

class FeatureFlag {

    public function isEnabled($feature_id)
    {
        // toggle base name
        $toggle = "feature-flags.{$feature_id}"; 
        $value = config("$toggle.value", false);

        $value = $this->isEnabledCookie($toggle, $value);

        
        return $value;
    }

    protected function isEnabledCookie($toggle, $def_val=FALSE)
    {
        // check if toggle can be overriden by cookie
        $override = config("$toggle.cookie_override", false);
        $cookie_name = str_replace('.','_', $toggle);
        if ($override && isset($_COOKIE[$cookie_name]))
        {
            return filter_var($_COOKIE[$cookie_name], FILTER_VALIDATE_BOOLEAN);
        }
        return $def_val;
    }

    public function getJavascriptFlags()
    {
        $settings = new Collection(config("feature-flags"));
        $settings = $settings->where('js_export', true);

        $results = [];
        foreach($settings as $key => $setting)
        {
            $results[$key] = $this->isEnabled($key);
        }

        return $results;
    }

}