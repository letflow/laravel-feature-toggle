<?php

use Kirschbaum\LaravelFeatureFlag\FeatureFlag;

if (! function_exists('toggle')) {
    /**
     * Check if FeatureToggle is enabled
     *
     * @param  string  $name
     * @return bool
     */
    function toggle($name)
    {
        return app('featureflag')->isEnabled($name);
    }
}

