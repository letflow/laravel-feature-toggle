<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Feature Flags
    |--------------------------------------------------------------------------
    |
    | Here you may configure feature flags. You may set a default and / or
    | include environment specific (e.g. dev, stage, production) settings.
    |
    */

    'feature-1' => [
        'value' => env('TOGGLE_FEATURE_1', false),
        'js_export'    => true,
    ],

    'feature-2' => [
        'cookie_override' => true,
        'js_export'      => true,
    ]
];
